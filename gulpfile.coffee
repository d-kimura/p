gulp        = require 'gulp'
jade        = require 'gulp-jade'
htmlmin     = require 'gulp-htmlmin'

browserSync = require 'browser-sync'
reload      = browserSync.reload

del         = require 'del'
concat      = require 'gulp-concat'

path =
  watch:
    html:  './src/jade/**/*.jade'
    images:'./src/images/**/*'
  src:
    html:  './src/jade/**/!(_)*.jade'
    images:'./src/images/**/*'
  dist:
    html:  './dist'
    css:   './dist/css'
    js:    './dist/js'
    images:'./dist/images'
  concat:
    src:
      js: [
        './node_modules/talkiejs/dist/talkie.min.js'
      ]
      css: [
        './node_modules/talkiejs/dist/talkie.min.css'
        './node_modules/talkiejs/dist/talkie-default.min.css'
        './node_modules/highlight.js/styles/monokai_sublime.css'
      ]
    dist:
      js:  'vender.js'
      css: 'vender.css'

gulp.task 'clean', ->
  del.sync path.dist.html

gulp.task 'jade', ->
  gulp.src path.src.html
    .pipe jade()
    .pipe htmlmin()
    .pipe gulp.dest path.dist.html
    .pipe reload
      stream: true

gulp.task 'concat:js', ->
  gulp.src path.concat.src.js
    .pipe gulp.dest path.dist.js

gulp.task 'concat:css', ->
  gulp.src path.concat.src.css
    .pipe gulp.dest path.dist.css

gulp.task 'images', ->
  gulp.src path.src.images
    .pipe gulp.dest path.dist.images

gulp.task 'serve', ->
  browserSync
    server:
      baseDir: path.dist.html

gulp.task 'build', ['clean', 'jade', 'concat:js', 'concat:css', 'images']

gulp.task 'default', ['build', 'serve'], ->
  gulp.watch path.watch.html, ['jade']
  gulp.watch path.watch.images, ['images']
